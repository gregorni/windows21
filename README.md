# Windows21
A Global Theme for KDE Plasma, similar to Windows 11 in design and Windows 10 in layout.

**__Note!!!__ This theme has been discontinued since I no longer use KDE! You may use it of course, but it will no longer recieve any updates! Feel free to redistribute it, or look for an alternative.

